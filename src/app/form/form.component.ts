import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  formGroup;
  formSubmitted: boolean = false;
  emailError: boolean = false;

  constructor(private http: HttpClient, private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      email: [null, Validators.required],
      nom: [null, Validators.required],
      prenom: [null, Validators.required]
    });
  }

  ngOnInit(): void {}

  onSubmit(formData: any) {
    var form: any = new FormData();

    form.append("MAIL", formData['email']);
    form.append("LASTNAME", formData['nom']);
    form.append("NAME", formData['prenom']);
  
    this.http.post<any>('https://mspr-ar-back.herokuapp.com/users/add', form).subscribe(
      (data) => {
        this.formSubmitted = true;
        this.emailError = false;
      },
      (error) => {
        this.emailError = true;
      }
    )    
  }

  resetFrom() {
    window.location.reload;
    this.formGroup.reset();
    this.formSubmitted = false;
    this.emailError = false;
  }
}
