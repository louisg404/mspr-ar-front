import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  faLeft = faArrowLeft;
  faRight = faArrowRight;

  clients : any[] = [];
  clientsOUVERT : any[] = [];
  clientsIDENTIFICATION : any[] = [];
  clientsPROPOSITION : any[] = [];
  clientsNEGOCIATION : any[] = [];
  clientsFERME_POSITIF : any[] = [];
  clientsFERME_NEGATIF : any[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.http.get<any>('https://mspr-ar-back.herokuapp.com/users').subscribe(data => {
      this.clients = data;
      data.forEach((element: any) => {
        if (element?.Statut == 'OUVERT') {
          this.clientsOUVERT.push(element);
          console.log(element);
        } else if (element?.Statut == 'IDENTIFICATION') {
          this.clientsIDENTIFICATION.push(element);
          console.log(element);
        } else if (element?.Statut == 'PROPOSITION') {
          this.clientsPROPOSITION.push(element);
          console.log(element);
        } else if (element?.Statut == 'NEGOCIATION') {
          this.clientsNEGOCIATION.push(element);
          console.log(element);
        } else if (element?.Statut == 'FERME_POSITIF') {
          this.clientsFERME_POSITIF.push(element);
          console.log(element);
        } else if (element?.Statut == 'FERME_NEGATIF') {
          this.clientsFERME_NEGATIF.push(element);
          console.log(element);
        }
      });
    });
  }

  putStatus(id: any, status: string) {
    var formData: any = new FormData();

    formData.append("ID", id);
    formData.append("STATUT", status);
  
    this.http.put<any>('https://mspr-ar-back.herokuapp.com/users/update', formData).subscribe(
      (data) => {
        console.log(data);
        window.location.reload();
      }
    )
  }
}
